package com.myex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradleExApplication {

	public static void main(String[] args) {
		SpringApplication.run(GradleExApplication.class, args);
	}

}
